export interface Todo {
  id: number;
  content: string;
}

export interface Meta {
  totalCount: number;
}

export interface Movie {
  id: number;
  title: string;
  director: string;
  summary: string;
  genre: string[]
}
